import * as express from "express";

export const helloController = (
    req: express.Request,
    res: express.Response,
  ) => {
    res.json({
      message: "Hello World!",
    });
};
