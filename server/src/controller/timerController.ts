import * as express from "express";

interface ITimerParams {
    timer: number;
  }

export const timerController =
(
  req: express.Request,
  res: express.Response,
) => {
  const params: ITimerParams = req.params;
  if (params.timer) {
    res.json({
      timer: params.timer,
    });
  } else {
    res.json({
      timer: 7,
    });
  }
};
