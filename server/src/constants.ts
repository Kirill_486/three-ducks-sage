import * as path from "path";

export const publicPath = path.join(__dirname, "..", "..", "client", "build");
export const fileName = "index.html";
