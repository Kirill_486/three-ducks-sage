import * as express from "express";
import { publicPath } from "./constants";

import { helloController } from "./controller/helloController";
import { homeController } from "./controller/homeController";
import { timerController } from "./controller/timerController";

class App {
  public express;

  constructor() {
    this.express = express();
    this.mountRoutes();
    this.setStaticFilesUrl();
  }

  private mountRoutes(): void {
    const router = express.Router();
    router.get("/hello", helloController);
    router.get("/timer/:timer", timerController);
    router.get("/", homeController);
    this.express.use("/", router);
  }

  private setStaticFilesUrl(): void {
    this.express.use(express.static(publicPath));
  }
}

export default new App().express;
