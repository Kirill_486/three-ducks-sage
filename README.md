## Three Ducks Saga

It is about Sagas (redux-saga)

## Get started

* install dependencies

```bash
server/npm i
client/npm i
```

* build client

```bash
client/npm run build
```

* run server

```bash
server/npm run dev
```
