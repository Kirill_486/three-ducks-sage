import { ActionCreator } from 'redux';
import { IAction, IStringAction } from 'src/types/action';
import { sagaActionTypes } from './appSagas';

export const sagaLong: ActionCreator<IAction> =
() => {
    return {
        type: sagaActionTypes.LONG
    }
}

export const sagaShort: ActionCreator<IAction> =
() => {
    return {
        type: sagaActionTypes.SHORT
    }
}

export const sagaLog: ActionCreator<IStringAction> =
(
    message: string
) => {
    return {
        type: sagaActionTypes.LOG_ARG,
        payload: message
    }
}