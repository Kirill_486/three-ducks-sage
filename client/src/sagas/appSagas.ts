import { takeLatest, takeEvery, call, put } from '@redux-saga/core/effects';
import { IAction, IStringAction } from 'src/types/action';
import { statusMessage, statusShow, statusHide } from 'src/store/actions/statusActions';
import { makeTimerRequest, ITimerResponceData } from 'src/api/timerRequest';
import { timerCount, timerShow, timerHide } from 'src/store/actions/timerActions';

export enum sagaActionTypes {
    LONG = 'SAGA_LONG',
    SHORT = 'SAGA_SHORT',
    LOG_ARG = 'LOG_ARG'
}

export enum statusMessages {
    LOADING = 'Loading...',
    TIMER = 'Timer...',
    DONE = 'Done!'
}

const delay = (ms: number) => new Promise(res => setTimeout(res, ms))

export function* longSaga (action: IAction) {
    yield call(console.warn, `hello long saga ${action.type}`);
    yield put(statusMessage(statusMessages.LOADING));
    yield put(statusShow());

    const timerResponse: ITimerResponceData = yield call(makeTimerRequest, true);
    yield put(statusMessage(statusMessages.TIMER));

    yield put(timerCount(timerResponse.timer));
    yield put(timerShow());

    for (let t = 1; t <= timerResponse.timer; t++) {
        yield call(delay, 1000);
        yield put(timerCount(timerResponse.timer - t));
    }

    yield put(statusMessage(statusMessages.DONE));

    yield call(delay, 2000);

    yield put(statusHide());
    yield put(timerHide());

    // yield call(console.log, timerResponse);
}

export function* shortSaga (action: IAction) {
    yield call(console.warn, `hello short saga ${action.type}`);
}

export function* logSaga(action: IStringAction) {
    yield call(console.log, `log saga ${action.type} - ${action.payload}`);
}

export function* rootSaga () {
    yield takeLatest(sagaActionTypes.LONG, longSaga);
    yield takeLatest(sagaActionTypes.SHORT, shortSaga);
    yield takeEvery(sagaActionTypes.LOG_ARG, logSaga);
}