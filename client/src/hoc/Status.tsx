import { IApplicationState } from 'src/types/types';
import { connect } from 'react-redux';
import { StatusBar } from 'src/components/StatusBar/StatusBar';
import { IStatusBarStateProps } from 'src/components/StatusBar/props';

const mapStateToProps =
(
    state: IApplicationState
): IStatusBarStateProps => {
    return {
        isShown: state.status.isShown,
        message: state.status.message
    }
}

const connectedStatus = connect(mapStateToProps)(StatusBar);
connectedStatus.displayName = 'Status';

export default connectedStatus;