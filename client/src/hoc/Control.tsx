import { connect } from "react-redux";
import { ControlPanel } from 'src/components/ControlPanel/ControlPanel';
import { sagaLong, sagaShort, sagaLog } from 'src/sagas/sagaActionCreators';
import { IControlPanelDispatchProps, IControlPanelStateProps } from 'src/components/ControlPanel/props';
import { IApplicationState } from 'src/types/types';

const mapStateToProps = (
    state: IApplicationState
): IControlPanelStateProps => {
    return {
        action1Title: 'LongSaga',
        action2Title: 'console.log',
        action3Title: 'log arg'
    }
}

const mapDispatchToProps = (
    dispatch: any
): IControlPanelDispatchProps => {
    return {
        action1: () => dispatch(sagaLong()),
        action2: () => dispatch(sagaShort()),
        action3: () => dispatch(sagaLog('arg'))
    }
}

const connectedControlPanel = connect(mapStateToProps, mapDispatchToProps)(ControlPanel);
connectedControlPanel.displayName = 'Control';

export default connectedControlPanel;