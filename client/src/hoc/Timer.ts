import { TimerBar } from '../components/TimerBar/TimerBar';
import { connect } from "react-redux";
import { IApplicationState } from 'src/types/types';
import { ITimerBarStateProps } from 'src/components/TimerBar/props';

const mapStateToProps =
(
    state: IApplicationState
):ITimerBarStateProps => {
    return {
        isShown: state.timer.isShown,
        count: state.timer.count
    }
}

const connectedTimer = connect(mapStateToProps)(TimerBar);

connectedTimer.displayName = 'Timer';

export default connectedTimer;