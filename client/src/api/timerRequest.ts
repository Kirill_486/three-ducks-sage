// const timer5secUrl = 'http://www.mocky.io/v2/5c7b996a2f00009e16e59f09';
// const timer2secUrl = 'http://www.mocky.io/v2/5c7b99fd2f00005313e59f11';

const timer = '/timer';
// const getHost = () => document.location.host;

export interface ITimerResponceData {
    timer: number;
}

const getTimerUrl = (sec: number) => `${timer}/${sec}`;

const getTimerRequestPromice =
(
    url: string
):Promise<ITimerResponceData> => {
    return fetch(url)
    .then((responce) => {
        if (responce.ok) {
            return responce.json();
        } else {
            throw responce;
        }
    })
    .catch((reason) => {
        console.error(reason);
    });
}

export const makeTimerRequest =
(
    isLong: boolean
) => {
    let timerUrl: string;
    if (isLong) {
        timerUrl = getTimerUrl(5);
        
    } else {
        timerUrl = getTimerUrl(2);        
    }
    console.log(timerUrl);
    const promice = getTimerRequestPromice(timerUrl);
    return promice;
}