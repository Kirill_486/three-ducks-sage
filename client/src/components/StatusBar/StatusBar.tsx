import * as React from 'react';
import { IStatusBarProps } from './props';

import './StatusBar.css';

export const StatusBar: React.SFC<IStatusBarProps> =
(
    props: IStatusBarProps
) => props.isShown ? (
    <div className="status-bar__container">
        <h2 className="status-bar__status">
            {`${props.message}`}
        </h2>        
    </div>
) : null; 

StatusBar.displayName = 'StatusBar';
StatusBar.defaultProps = {
    isShown: false,
    message: 'default'
}
