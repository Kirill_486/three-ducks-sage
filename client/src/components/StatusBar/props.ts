export interface IStatusBarStateProps {
    isShown: boolean;
    message: string;
}

// export interface IStatusBarDispatchProps {    
//     markerClick: () => void;
// }

// tslint:disable-next-line:no-empty-interface
export interface IStatusBarProps extends 
IStatusBarStateProps
// IStatusBarDispatchProps 
{};