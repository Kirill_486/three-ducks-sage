import * as React from 'react';
import './TimerBar.css';
import { ITimerBarProps } from './props';

export const TimerBar: React.SFC<ITimerBarProps> =
(
    props: ITimerBarProps
) => (
    <div className="timer-bar__container">
        <h2 className="timer-bar__timer">
            {`Timer: ${props.isShown ? props.count : ''}`}
        </h2>        
    </div>
);

TimerBar.displayName = 'TimerBar';
TimerBar.defaultProps = {
    count: -1,
    isShown: false
}