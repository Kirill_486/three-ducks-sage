export interface ITimerBarStateProps {
    isShown: boolean;
    count: number;
}

// tslint:disable-next-line:no-empty-interface
export interface ITimerBarProps extends 
ITimerBarStateProps { }