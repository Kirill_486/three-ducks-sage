export interface IControlPanelStateProps {
    action1Title: string;
    action2Title: string;
    action3Title: string;
}

export interface IControlPanelDispatchProps {
    action1: () => void,
    action2: () => void,
    action3: () => void
}

// tslint:disable-next-line:no-empty-interface
export interface IControlPanelProps extends
IControlPanelStateProps,
IControlPanelDispatchProps 
{}