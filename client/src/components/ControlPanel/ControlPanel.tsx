import * as React from 'react';

import './ControlPanel.css';
import { IControlPanelProps } from './props';

export const ControlPanel: React.SFC<IControlPanelProps> =
(props) => {
    return (
        <div className="control__container">
            <button 
                className="control__action action1"
                onClick={props.action1}
            >
                {props.action1Title}
            </button>
            <button 
                className="control__action action2"
                onClick={props.action2}
            >
                {props.action2Title}
            </button>
            <button 
                className="control__action action3"
                onClick={props.action3}
            >
                {props.action3Title}
            </button>
        </div>
    );
};

ControlPanel.displayName = 'Control Panel';
ControlPanel.defaultProps = {
    action1Title: 'Action1',
    action2Title: 'Action2',
    action3Title: 'Action3',
    action1: () => console.log('action1'),
    action2: () => console.log('action2'),
    action3: () => console.log('action3')
}

