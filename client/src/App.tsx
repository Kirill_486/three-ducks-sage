import * as React from 'react';
import './App.css';

import Timer from './hoc/Timer';
import Status from './hoc/Status';
import Control from './hoc/Control';

import store from './store/configureStore';

import logo from './logo.svg';
import { Provider } from 'react-redux';

class App extends React.Component {
  public render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Three Ducks Saga</h1>
        </header>
        <Provider store={store}>
          <div className="three-ducks-container">
            <Timer />
            <Status />
            <Control />
          </div>          
        </Provider>
      </div>
    );
  }
}

export default App;
