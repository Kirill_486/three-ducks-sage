import {createStore, combineReducers, Store, applyMiddleware} from 'redux';
import statusReducer from './reducers/statusReducer';
import timerReducer from './reducers/timerReducer';
import { IApplicationState } from 'src/types/types';
import createSagaMiddleware from 'redux-saga';
import { rootSaga } from '../sagas/appSagas';

const resultReducer = combineReducers({
    timer: timerReducer,
    status: statusReducer
});

const sagaMiddleware = createSagaMiddleware();

const appStore: Store<IApplicationState> = 
createStore(
    resultReducer,
    applyMiddleware(sagaMiddleware)
);

sagaMiddleware.run(rootSaga);

export default appStore;