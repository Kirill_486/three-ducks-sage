import { statusActionTypes } from './../reducers/statusReducer';
import { IAction, IStringAction } from './../../types/action';
import { ActionCreator } from 'redux';

export const statusShow: ActionCreator<IAction> =
() => {
    return {
        type: statusActionTypes.SHOW
    }
};

export const statusHide: ActionCreator<IAction> =
() => {
    return {
        type: statusActionTypes.HIDE
    }
};

export const statusMessage: ActionCreator<IStringAction> =
(
    message: string
) => {
    return {
        type: statusActionTypes.SET_MESSAGE,
        payload: message
    }
};
