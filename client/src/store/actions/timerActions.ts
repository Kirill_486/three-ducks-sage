import { IAction, INumberAction } from './../../types/action';
import { ActionCreator } from 'redux';
import { timerActionTypes } from '../reducers/timerReducer';

export const timerShow: ActionCreator<IAction> = () => {
    return {
        type: timerActionTypes.SHOW
    }
};

export const timerHide: ActionCreator<IAction> = () => {
    return {
        type: timerActionTypes.HIDE
    }
};

export const timerCount: ActionCreator<INumberAction> =
(
    count: number
) => {
    return {
        type: timerActionTypes.COUNT,
        payload: count
    }
};
