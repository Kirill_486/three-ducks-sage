import { IStatusAction, IStringAction } from './../../types/action';
import { IStatusState } from './../../types/types';
import { statusDefaultState } from './../../types/defaults';

export enum statusActionTypes {
    SHOW = 'STATUS_SHOW',
    HIDE = 'STATUS_HIDE',
    SET_MESSAGE = 'STATUS_SET_MESSAGE'    
};

const timerReducer =
(
    state: IStatusState = statusDefaultState,
    action: IStatusAction
): IStatusState => {
    switch (action.type) {
        case statusActionTypes.SHOW: {
            return {
                ...state,
                isShown: true
            }
        }
        case statusActionTypes.HIDE: {
            return {
                ...state,
                isShown: false
            }
        }
        case statusActionTypes.SET_MESSAGE: {
            const stringMessage = action as IStringAction;
            return {
                ...state,
                message: stringMessage.payload
            }
        }
        default: return state;
    }
}

export default timerReducer;