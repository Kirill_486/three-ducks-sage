import { ITimerAction, INumberAction } from './../../types/action';
import { timerDefaultState } from './../../types/defaults';
import { ITimerState } from './../../types/types';

export enum timerActionTypes {
    SHOW = 'TIMER_SHOW',
    HIDE = 'TIMER_HIDE',
    COUNT = 'TIMER_COUNT'
}

const timerReducer =
(
    state: ITimerState = timerDefaultState,
    action: ITimerAction
): ITimerState => {
    switch (action.type) {
        case timerActionTypes.SHOW: {
            return {
                ...state,
                isShown: true
            }
        }
        case timerActionTypes.HIDE: {
            return {
                ...state,
                isShown: false
            }
        }
        case timerActionTypes.COUNT: {
            const numberAction = action as INumberAction;
            return {
                ...state,
                count: numberAction.payload
            }
        }
        default: return state;
    }
}

export default timerReducer;