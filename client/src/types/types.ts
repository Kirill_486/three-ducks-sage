export interface ITimerState {
    isShown: boolean;
    count: number;
}

export interface IStatusState {
    isShown: boolean;
    message: string;
}

export interface IApplicationState {
    timer: ITimerState,
    status: IStatusState
}