import { ITimerState, IStatusState } from "./types";

export const timerDefaultState: ITimerState = {
    isShown: false,
    count: -1    
};

export const statusDefaultState: IStatusState = {
    isShown: false,
    message: 'Status'
}