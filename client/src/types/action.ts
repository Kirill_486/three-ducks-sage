import { Action } from 'redux';

// tslint:disable-next-line:no-empty-interface
export interface IAction extends Action { }

export interface IStringAction extends Action {
    payload: string;
}

export interface INumberAction extends Action {
    payload: number;
}

export type ITimerAction = IAction | INumberAction;
export type IStatusAction = IAction | IStringAction;